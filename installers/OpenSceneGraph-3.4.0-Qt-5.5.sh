#!/bin/bash
cd $HOME/opt
wget http://gmrv.es/~ptoharia/OpenSceneGraph-3.4.0-Qt5.5.tgz
tar zxvf OpenSceneGraph-3.4.0-Qt5.5.tgz
rm OpenSceneGraph-3.4.0-Qt5.5.tgz

echo "module load OpenSceneGraph-3.4.0-Qt5.5" >> $HOME/.bashrc
source $HOME/.bashrc
