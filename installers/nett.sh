#!/bin/bash
mkdir -p cd $HOME/opt/src && cd $HOME/opt/src
git clone https://devhub.vr.rwth-aachen.de/VR-Group/nett.git
cd nett
mkdir Release && cd Release
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$HOME/opt/nett
make -j install
