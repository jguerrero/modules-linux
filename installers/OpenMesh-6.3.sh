#!/bin/bash
mkdir -p cd $HOME/opt/src && cd $HOME/opt/src
VER=6.3
git clone https://www.graphics.rwth-aachen.de:9000/OpenMesh/OpenMesh.git OpenMesh-$VER
cd OpenMesh-$VER
git checkout OpenMesh-$VER
git apply $HOME/opt/modules/installers/OpenMesh-6.3.patch
mkdir Release && cd Release
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=$HOME/opt/OpenMesh-$VER
make -j install
