#!/bin/bash
cd $HOME/opt/src
git clone https://github.com/google/protobuf
cd protobuf
git checkout v3.1.0
mkdir -p Release
cd Release
cmake ../cmake/ -DCMAKE_BUILD_TYPE=Release -Dprotobuf_BUILD_TESTS=OFF \
      -DCMAKE_INSTALL_PREFIX=~/opt/protobuf-3.1.0 \
      -DCMAKE_CXX_FLAGS="-fPIC"
make install -j4
echo "module load protobuf-3.1.0" >> $HOME/.bashrc
source $HOME/.bashrc
