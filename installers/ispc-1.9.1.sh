#!/bin/bash
# Download and install ispc 1.9.1
cd $HOME/opt

VERSION=1.9.1

# Old: to compile it from scratch
# sudo apt-get install libncurses5-dev llvm clang libclang-dev
# git clone https://github.com/ispc/ispc.git ispc-${VERSION}
# cd ispc-${VERSION}
# git checkout v$VERSION
# make
wget http://netix.dl.sourceforge.net/project/ispcmirror/v1.9.1/ispc-v1.9.1-linux.tar.gz
tar zxvf ispc-v1.9.1-linux.tar.gz

# # Install only the executable
# mkdir $HOME/opt/ispc-${VERSION}
# cp ispc $HOME/opt/ispc-${VERSION}

echo "module load ispc-"${VERSION} >> $HOME/.bashrc
source $HOME/.bashrc
