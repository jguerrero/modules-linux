#!/bin/bash
sudo apt-get install libqt5designer5 qttools5-dev
mkdir -p ~/opt/src && cd ~/opt/src
wget http://gmrv.es/~ptoharia/libQGLViewer-2.6.3.tar.gz
tar zxvf libQGLViewer-2.6.3.tar.gz
cd libQGLViewer-2.6.3
qmake
make -j4 
INSTALL_ROOT=~/opt/libQGLViewer-2.6.3 make install

echo "module load libQGLViewer-2.6.3" >> $HOME/.bashrc
source $HOME/.bashrc