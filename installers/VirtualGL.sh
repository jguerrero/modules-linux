#!/bin/bash
sudo apt-get install libturbojpeg libjpeg-turbo8-dev
mkdir -p ~/opt/src && cd ~/opt/src
wget http://netassist.dl.sourceforge.net/project/virtualgl/2.4.1/VirtualGL-2.4.1.tar.gz
tar zxvf VirtualGL-2.4.1.tar.gz
mkdir -p VirtualGL-2.4.1/Release && cd VirtualGL-2.4.1/Release 
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=~/opt/VirtualGL-2.4.1 -DTJPEG_INCLUDE_DIR=/usr/include/ -DTJPEG_LIBRARY=/usr/lib/x86_64-linux-gnu/libturbojpeg.so.0
make -j4 install

echo "module load VirtualGL-2.4.1" >> $HOME/.bashrc
source $HOME/.bashrc
