# Introduction

Modules-Linux allows to make available diverse libraries or programs
on top of system installed versions, allowing to dynamically load and
unload the versions needed for building or running other
applications. Note these modules do NOT overwrite or modify existing
system installations. In case you have not installed it previously, it can be
downloaded from Ubuntu repositories with:

```bash
sudo apt-get install environment-modules
```

# Features

* Automatic installers for the download and installling of required versions
* Module files ready to be loaded and unloaded for diverse libraries such as
 Qt 5.5, OpenSceneGraph 3.4.0

# Setup steps

This project assumes you are using bash and you are using Ubuntu 14.04.

First, download the repository to $HOME/opt and name it modules.

```bash
git clone git@gitlab.gmrv.es:common/modules-linux.git $HOME/opt/modules
```

If you had already download it just move the whole directory to
$HOME/opt.

```bash
mv modules-linux $HOME/opt/modules
```

Prior to start running installers and loading/unloading modules you need to
install GNU Modules and create a directory structure (based on $HOME/opt) to
hold the software and modules. This project includes a script that does the
whole process:

```bash
./setup.sh
```

# Installing Modules

Run the desired scripts located on the installers folder.

Example:

```bash
cd installers
./Qt-5.5.sh
```

# Loading/Unloading Modules

Modules can be simply loaded and unloaded on demand.

## Load

In order to load a module, execute this command

```bash
module load <module_name>
```

Example:

```bash
module load Qt-5.5
```

## Unload

To unload an already loaded module:

```bash
module unload <module_name>
```

Example:

```bash
module unload Qt-5.5
```

## List loaded modules

To list the already loaded modules:

```bash
module list
```